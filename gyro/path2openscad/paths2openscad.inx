<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <_name>Paths to OpenSCAD</_name>
  <id>command.extrude.openscad</id>
  <dependency type="extension">org.inkscape.output.svg.inkscape</dependency>
  <dependency type="executable" location="extensions">paths2openscad.py</dependency>
  <dependency type="executable" location="extensions">inkex.py</dependency>
  <dependency type="executable" location="extensions">simpletransform.py</dependency>
  <dependency type="executable" location="extensions">cubicsuperpath.py</dependency>
  <dependency type="executable" location="extensions">cspsubdiv.py</dependency>
  <dependency type="executable" location="extensions">bezmisc.py</dependency>

<param name="tab" type="notebook">
  <page name="splash" _gui-text="Paths to OpenSCAD">
    <_param name="header" type="description" xml:space="preserve">The Height parameter specifies how high
in millimeters to extrude the polygons.

The Smoothing parameter describes how
smoothly to render curves.  Use
smaller values for smoother curves.

The Line width parameter sets width only
when the line is not a closed loop.

Force line output will save closed
shapes as a continuous line instead
of a polygon.
</_param>
    <param name="fname" type="string" _gui-text="Output file">~/inkscape.scad</param>
    <param name="height" type="float" min="0" max="1000" precision="2" _gui-text="Height (mm)">5.0</param>
    <param name="smoothness" type="float" min="0.0001" max="5" _gui-text="Smoothing">0.2</param>
	<param name="line_width" type="float" min="0.1" max="10" _gui-text="Line width">1.0</param>
	<param name="force_line" type="boolean" _gui-text="Force line output">False</param>
  </page>
  <page name="info" _gui-text="About...">
    <_param name="aboutpage" type="description" xml:space="preserve">
This extension converts Inkscape paths to
extruded polygons in OpenSCAD.  Before
using, first convert objects to paths
with the "Path &gt; Object to Path"
menu item.

Note that the paths must be polygonal.
Non-polygonal paths will not render well
in OpenSCAD.  Thus, while you can convert
text to a path correctly, other shapes may
need to be combined before export
(e.g. "o" style shapes with holes).

Inkscape's units of pixels are converted
to millimeters using the SVG Standard's
definition of 90 pixels = 1 inch.

v0.9
Dan Newman (dan newman @ mtbaldy us)
</_param>
  </page>
</param>

  <effect needs-live-preview="false">
    <object-type>all</object-type>
    <effects-menu>
      <submenu _name="Generate from Path"/>
    </effects-menu>
  </effect>
  <script>
    <command reldir="extensions" interpreter="python">paths2openscad.py</command>
  </script>
</inkscape-extension>
