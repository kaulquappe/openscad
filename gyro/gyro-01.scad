
// Module names are of the form poly_<inkscape-path-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-path-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_paths variable as well. If it exists then it 
//  defines the nested paths. Both must be used in the 
//  polygon(points, paths) variant of the command.

profile_scale = 25.4/90; //made in inkscape in mm

// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);

height = 1;
width = 1.0;


path4487_0_points = [[-72.548509,84.809764],[-70.683464,83.681034],[-69.208759,82.260917],[-68.076098,80.585529],[-67.237186,78.690984],[-66.247416,74.388883],[-65.853078,69.643531],[-65.667798,64.743848],[-65.305204,59.978750],[-64.378922,55.637156],[-63.583657,53.715460],[-62.502579,52.007984],[-58.710736,47.131322],[-54.649966,42.965989],[-50.344458,39.471212],[-45.818400,36.606217],[-41.095980,34.330232],[-36.201387,32.602485],[-31.158808,31.382203],[-25.992433,30.628613],[-20.726449,30.300943],[-15.385044,30.358420],[-9.992407,30.760271],[-4.572726,31.465724],[6.251014,33.624344],[16.892671,36.508100],[27.158737,39.790810],[36.855709,43.146293],[45.790078,46.248368],[53.768339,48.770854],[60.596986,50.387570],[63.519733,50.754332],[66.082513,50.772334],[68.261135,50.400803],[70.031413,49.598966],[71.369157,48.326050],[72.250181,46.541284],[72.548509,43.411012],[72.536891,40.313816],[71.615073,34.235051],[69.547255,28.337785],[66.395964,22.654815],[62.223726,17.218938],[57.093069,12.062949],[51.066519,7.219645],[44.206603,2.721823],[36.575849,-1.397722],[28.236783,-5.106192],[19.251933,-8.370792],[9.683826,-11.158724],[-0.405012,-13.437194],[-10.952054,-15.173403],[-21.894772,-16.334556],[-33.170639,-16.887856],[-37.902952,-17.024314],[-42.180735,-17.567590],[-46.026851,-18.489769],[-49.464160,-19.762936],[-52.515524,-21.359175],[-55.203802,-23.250572],[-57.551857,-25.409212],[-59.582548,-27.807178],[-61.318738,-30.416556],[-62.783285,-33.209431],[-64.988900,-39.234010],[-66.382281,-45.657594],[-67.146313,-52.256861],[-67.463885,-58.808490],[-67.517884,-65.089160],[-67.566709,-75.944335],[-67.927309,-80.072197],[-68.755885,-83.035814],[-69.402816,-84.011242],[-70.235323,-84.611864],[-71.276267,-84.809764],[-72.548509,-84.577026],[-72.548509,84.809764]];

module poly_path4487(h, w, res=4)  {
  scale([profile_scale, -profile_scale, 1])
 // union()  {
 //   linear_extrude(height=h)
      polygon(path4487_0_points);
//  }
}

// The shapes
//poly_path4487(height, width);
scale([profile_scale, profile_scale, profile_scale])
rotate([0,180,0]) 
rotate_extrude($fn=100)
translate([72.548509,0,0]) polygon(path4487_0_points);
