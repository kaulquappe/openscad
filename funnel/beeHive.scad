//unit size
size = 7;

//wall thickness
wall=3;

//height
hexaHeight=2.5;

//number of hexagons in each direction
hexaNum=2;

//hole (to attach to bracelet)
holeX=3;
holeY=3;
holeZ=4.5;

//star(10,4);
//regHexagon(10,4);

translate([0,0,hexaHeight/2]) {
	beeHive(size, wall, hexaNum, hexaHeight, holeX, holeY, holeZ);
}

//----------------------

module beeHive(size, wall, hexaNum, hexaHeight, holeX, holeY, holeZ) {

	sizeInt=size-wall;

	module hollowHexagon(size, hexaHeight) 
    {
		difference() 
        {
			regHexagon(size, hexaHeight);
			regHexagon(sizeInt, hexaHeight);
		}
	}

	module centerHexagon(size, wall, hexaHeight, holeX, holeY, holeZ) 
    {
		union() 
        {
			regHexagon(size, hexaHeight);
			translate([0,0,hexaHeight]) 
            {
				box(holeX, holeY, holeZ);
			}
		}
	}

	union() {

		//CENTER HEXAGON
		//centerHexagon(size, wall, hexaHeight, holeX, holeY, holeZ);
		
		//OTHER HEXAGONS
		for (h=[1,hexaNum]) {

			//TOP
			//translate([0,size*1.75*h,0]) {
			translate([0,size*1.75*h-wall*h,0]) {
				hollowHexagon(size, hexaHeight);
			}

			//BOTTOM
			//translate([0,-size*h*1.75,0]) {
			translate([0,-size*h*1.75+wall*h,0]) {
				hollowHexagon(size, hexaHeight);
			}

			//TOP RIGHT
			//translate([size*h*1.5,(size*h*1.75)/2,0]) {
			translate([size*h*1.5-wall*h/1.5,size*h*1.75/2 - wall*h/2,0]) {
				hollowHexagon(size, hexaHeight);
			}

			//TOP LEFT
			//translate([-(size*h*1.5),(size*h*1.75)/2,0]) {
			translate([-(size*h*1.5-wall*h/1.5),size*h*1.75/2 - wall*h/2,0]) {
				hollowHexagon(size, hexaHeight);
			}

			//BOTTOM RIGHT
			//translate([size*h*1.5,(size*h*1.75)/2,0]) {
			translate([size*h*1.5-wall*h/1.5,-(size*h*1.75/2 - wall*h/2),0]) {
				hollowHexagon(size, hexaHeight);
			}

			//BOTTOM LEFT
			//translate([size*h*1.5,(size*h*1.75)/2,0]) {
			translate([-(size*h*1.5-wall*h/1.5),-(size*h*1.75/2 - wall*h/2),0]) {
				hollowHexagon(size, hexaHeight);
			}

		}

		//CORNER HEXAGONS
		//RIGHT		
		translate([size*3-wall*1.5,0,0]) {
			hollowHexagon(size, hexaHeight);
		}

		//LEFT
		translate([-(size*3-wall*1.5),0,0]) {
			hollowHexagon(size, hexaHeight);
		}
		//TOP LEFT
		translate([-(size*1.5-wall/1.5),size*3*1.75/2 - wall*3/2,0]) {
			hollowHexagon(size, hexaHeight);
		}
		//TOP RIGHT	
		translate([size*1.5-wall/1.5,size*3*1.75/2 - wall*3/2,0]) {
			hollowHexagon(size, hexaHeight);
		}
		//BOTTOM RIGHT	
		translate([size*1.5-wall/1.5,-(size*3*1.75/2 - wall*3/2),0]) {
			hollowHexagon(size, hexaHeight);
		}
		//BOTTOM LEFT
		translate([-(size*1.5-wall/1.5),-(size*3*1.75/2 - wall*3/2),0]) {
			hollowHexagon(size, hexaHeight);
		}
	}
}

//----------------------
module box(xBox, yBox, zBox) {
	scale ([xBox, yBox, zBox]) {
		cube(1, true);
	}
}

module cone(height, radius) {
	cylinder(height, radius, 0);
}

module oval(xOval, yOval, zOval) {
	scale ([xOval/100, yOval/100, 1]) {
		cylinder(zOval, 50, 50);
	}
}

module tube(height, radius, wall) {
	difference(){
		cylinder(height, radius, radius);
		cylinder(height, radius-wall, radius-wall);
	}
}

module octagon(side, height) {
	intersection(){
		box(side, side, height);
		rotate(45, [0,0,1]){
			box(side, side, height);
		}
	}
}

module regHexagon(size, height) {
	boxWidth=size*1.75;
	union(){
		box(size, boxWidth, height);
		rotate(60, [0,0,1]){
			box(size, boxWidth, height);
		}
		rotate(-60, [0,0,1]){
			box(size, boxWidth, height);
		}
	}
}

module star(side, height) {
	union(){
		box(side, side, height);
		rotate(45, [0,0,1]){
			box(side, side, height);
		}
	}
}
