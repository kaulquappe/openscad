myCylinderDia=10;         // cylinder used to cut holes out
myCylinderDistance=2;
//
myRasterSize=120.0;       // size of the square raster: make sure myRoundRasterDia/(myCylinderDia+myCylinderDistance) == 0 !!
myRasterHeight=5.0;
//
myRoundRasterDia=120.0;   // diameter of the round raster: make sure myRoundRasterDia/(myCylinderDia+myCylinderDistance) == 0 !!
myRoundRasterHeight=5.0;
//
nrSides=16;                // set this to 6 if you want a beehive pattern or to a higher value if you want rounder holes.
//
roundRaster(myCylinderDia, myCylinderDistance, myRoundRasterDia, myRoundRasterHeight, nrSides);
//
translate([myRasterSize*0.6, -myRasterSize/2, 0])
    squareRaster(myCylinderDia, myCylinderDistance, myRasterSize, myRasterHeight, nrSides);

/////////////////////////////////////////////////////////////////////////////////////
//
module squareRaster(cylinderDia, cylinderDistance, rasterSize, rasterHeight, fn)
{
    cylinderRad=cylinderDia/2.0;
    cylinderHeight=rasterHeight+1;
    yStepSize=cylinderDia+cylinderDistance;
    yNrSteps= rasterSize/yStepSize;
    // the distance between center of cylinders in x direction is determined by 
    // the height of an  equilateral triangle (sl = sidelen):
    // xStepSize = sqrt( sl^2 - (sl/2)^2 )
    xStepSize = sqrt( pow(yStepSize,2) - pow(yStepSize/2.0,2) );
    xNrSteps= rasterSize/xStepSize;
   
    translate([0,0,rasterHeight/2.0])
    difference()
    {
      translate([0, 0, -rasterHeight/2.0]) 
      cube(size=[rasterSize,rasterSize,rasterHeight], center=false);

      for ( xstep=[0:(xNrSteps/2.0)] )
      {
          translate([(2*xstep)*xStepSize, 0, 0])
          for ( ystep=[0:yNrSteps] )
          {
              translate([cylinderRad+cylinderDistance/2, ystep*yStepSize+cylinderRad+cylinderDistance/2, 0])
              cylinder(h=cylinderHeight, d=cylinderDia, center=true, $fn=fn);
          }
          translate([(2*xstep+1)*xStepSize, 0, 0])
          for ( ystep=[0:yNrSteps] )
          {
              translate([cylinderRad+cylinderDistance/2, ystep*yStepSize, 0])
              cylinder( h=cylinderHeight, d=cylinderDia, center=true, $fn=fn);
          }
      }
    }
}
/////////////////////////////////////////////////////////////////////////////////////
//
module roundRaster(cylinderDia, cylinderDistance, roundRasterDia, roundRasterHeight, fn)
{
    cylinderRad=cylinderDia/2.0;
    roundRasterRad=roundRasterDia/2.0;
    cylinderHeight=roundRasterHeight+1;
    yStepSize=cylinderDia+cylinderDistance;
    yNrSteps= roundRasterDia/yStepSize;
    // the distance between center of cylinders in x direction is determined by 
    // the height of an  equilateral triangle (sl = sidelen):
    // xStepSize = sqrt( sl^2 - (sl/2)^2 )
    xStepSize = sqrt( pow(yStepSize,2) - pow(yStepSize/2.0,2) );
    xNrSteps= roundRasterDia/xStepSize;
    
    translate([-roundRasterRad,-roundRasterRad,roundRasterHeight/2.0])
    difference()
    {
        translate([roundRasterRad, roundRasterRad, 0]) 
            cylinder(h=roundRasterHeight, r=roundRasterRad, center=true);

        for ( xstep=[0:(xNrSteps/2.0)] )
        {
            translate([(2*xstep)*xStepSize, 0, 0])
            for ( ystep=[0:yNrSteps] )
            {
                translate([cylinderRad+cylinderDistance/2, ystep*yStepSize+cylinderRad+cylinderDistance/2, 0])
                cylinder(h=cylinderHeight, d=cylinderDia, center=true, $fn=fn);
            }
            translate([(2*xstep+1)*xStepSize, 0, 0])
            for ( ystep=[0:yNrSteps] )
            {
                translate([cylinderRad+cylinderDistance/2, ystep*yStepSize, 0])
                cylinder( h=cylinderHeight, d=cylinderDia, center=true, $fn=fn);
            }
        }
    }
}

//// holes beside each other: 
//~ module roundRaster(cylinderRad, cylinderDistance, stepSize, nrSteps )
//~ {
  //~ for ( step=[0:nrSteps-1] )
  //~ {
      //~ translate([step*stepSize, 0, 0])
      //~ for ( step=[0:nrSteps-1] )
      //~ {
          //~ translate([cylinderRad+cylinderDistance/2, step*stepSize+cylinderRad+cylinderDistance/2, 0])
          //~ cylinder(h=cylinderHeight, r=cylinderRad, center=true);

      //~ }
  //~ }
//~ }
