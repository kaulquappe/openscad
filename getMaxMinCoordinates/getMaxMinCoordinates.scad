// this example shows how to get the maximum and mimimum x and y values of an vector (polygon)

vector=[[0,0],[100,0],[0,-100],[20,20],[-55,-20],[20,555]];

function flatten(vector) = [ for (a = vector) for (b = a) b ] ;
function getXcoordinates(vector) = [ for (a = vector) a[0] ] ;
function getYcoordinates(vector) = [ for (a = vector) a[1] ] ;
    
xCoords = getXcoordinates(vector);
yCoords = getYcoordinates(vector);

echo( "vector:                    ", vector );

echo( "flat vector:               ", flatten(vector) );
echo( "flat X coordinates:        ", getXcoordinates(vector) );
echo( "flat Y coordinates:        ", getYcoordinates(vector) );

echo( "max X coordinate:", max(getXcoordinates(vector)) );
echo( "min X coordinate:", min(getXcoordinates(vector)) );

echo( "max Y coordinate:", max(getYcoordinates(vector)) );
echo( "min Y coordinate:", min(getYcoordinates(vector)) );
