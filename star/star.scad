///////////////////////////////////////////////////////////////
// variables

// inside radius of the star
insideRadius = 80.0;
//outside radius of the star
outsideRadius = 150.0; 

///////////////////////////////////////////////////////////////
// main

makeStar(outsideRadius,insideRadius);

///////////////////////////////////////////////////////////////
// modules

module makeStar( Rout, Rinn )
{
    star = [
           [Rout*cos(90.0+1*72.0), Rout*sin(90.0+1*72.0)], 
           [Rinn*cos(126.0+1*72.0), Rinn*sin(126.0+1*72.0)],

           [Rout*cos(90.0+2*72.0), Rout*sin(90.0+2*72.0)], 
           [Rinn*cos(126.0+2*72.0), Rinn*sin(126.0+2*72.0)],

           [Rout*cos(90.0+3*72.0), Rout*sin(90.0+3*72.0)], 
           [Rinn*cos(126.0+3*72.0), Rinn*sin(126.0+3*72.0)],

           [Rout*cos(90.0+4*72.0), Rout*sin(90.0+4*72.0)], 
           [Rinn*cos(126.0+4*72.0), Rinn*sin(126.0+4*72.0)],

           [Rout*cos(90.0+5*72.0), Rout*sin(90.0+5*72.0)], 
           [Rinn*cos(126.0+5*72.0), Rinn*sin(126.0+5*72.0)] 
           ];

    echo(star);
    polygon(star);
}


